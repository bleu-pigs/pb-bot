------  INITIALIZATION
----  variable localization
local component = component
local PBCore = PBCore
local Construct = PBCore.ObjectsManager
local Enums = PBCore:depend("EnumsManager").Enums
local Utilities = PBCore:depend("Utilities")
local Scheduler = PBCore:depend("ThreadManager").Scheduler
PBCore:depend("LuaSignal")
