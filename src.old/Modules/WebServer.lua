--  Module/WebServer.lua 1.0.0
--      Implements the PigBot WebLit Server, granting the ability to communicate
--      with ROBLOX game servers
------  MODULE INITIALIZATION
local webLit = require("weblit").app
------  MODULE API
--  override root
root = webLit
------  FINALIZE MODULE
ready()
pbReady:wait()
--  initialize WebServer
webLit.use(
    require("weblit-logger")
)
webLit.start()