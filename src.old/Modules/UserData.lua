--  Modules/UserData.lua
----  Localization, Variables, and Constants
local JSON = Utilities.JSON
--  constants
local USER_DATA = {}
local FILE_EXTENSION = "UserData/%s.userdata"
local FILE_PERMISSIONS = "0666"
local FILE_READWRITE = "w+"
local FILE_READONLY = "r"
local GUILD = Client:getGuild(BotConfig.Management.ManagedGuild)
----- MODULE API
----  UserData FUNCTIONS
--  UserData  getUserData(string memberId)
--    Returns the UserData from cache. If none exists, will then try to open the associate flat
--    database for the memberId. If none exists, creates on the spot.
local function getUserData(memberId)
  Debugging.checkType(
    memberId,
    "string",
    1
  )
  ------  logic
  --  first, let's see if we can get the UserData from cache.
  local returningData = USER_DATA[memberId]
  if returningData == nil then
    --  its not in the cache. lets see if we have a flat database on the member.
    --  check to see if the file exists
    local userData
    local fileExists = FileSystem.readFile(FILE_EXTENSION:format(memberId))
    if fileExists == nil then
      --  file doesnt exist, create it
      userData = {}
      fileExists = FileSystem.writeFile(
        FILE_EXTENSION:format(
          memberId
        ),
        "{}"
      )
    else
      --  file exists, go ahead and read it
      userData = FileSystem.readFile(
        FILE_EXTENSION:format(
          memberId
        ),
        FILE_PERMISSIONS
      )
      pcall(
        function()
          userData = JSON.decode(userData)
        end
      )
      if userData == nil then
        userData = {}
      end
    end
    --  finalize returningData
    USER_DATA[memberId] = {
      fileExists == false,
      userData
    }
    returningData = USER_DATA[memberId]
  end
  return returningData
end
--  boolean, string  saveUserData(string memberId)
--    Saves the UserData for memberId to its associated flat database. Will fail if no cache is
--    found in USER_DATA
local function saveUserData(memberId)
  Debugging.checkType(
    memberId,
    "string",
    1
  )
  ------  logic
  --  first, let's see if we can get the UserData from cache.
  local savingData = USER_DATA[memberId]
  local __type_savingData = type(savingData)
  if __type_savingData == "nil" then
    --  uh oh, we dont have data to save. terminate now.
    return false, "failed to find savedata for member"
  end
  --  now, lets save our UserData
  local jsonUserData = JSON.encode(savingData[2])
  FileSystem.writeFile(
    FILE_EXTENSION:format(
      memberId
    ),
    jsonUserData
  )
end
--  void  updateUserData(member)
--    Updates the UserData according to member.status
local function updateUserData(member)
  Debugging.assert(
    member ~= nil,
    "bad argument #1 (member expected, got nil)"
  )
  ----  main function logic
  if member.status == "online" then
    --  member is online, load saved data
    if USER_DATA[member.id] == nil then
      getUserData(member.id)
      return
    end
  end
  --  first, check to see if the UserData even exists
  local userData = USER_DATA[member.id]
  local __type_userData = type(userData)
  if __type_userData == "nil" then
    --  userData is nil, do nothing.
    return
  end
  --  userData exists, save it.
  saveUserData(member.id)
  --  remove it from cache
  USER_DATA[member.id] = nil
  userData = nil
end
--  vararg  UserData:get(memberId, key)
--    Returns the stored key associated with memberId
function declare__get(self, memberId, key)
  Debugging.checkSelf(
    self,
    root
  )
  Debugging.checkType(
    memberId,
    "string",
    1
  )
  local __type_key = type(key)
  Debugging.assert(__type_key ~= "nil", "bad argument #1 (key cannot be nil, got ".. __type_key ..")")
  ----  return results
  local userData = getUserData(memberId)
  return userData[2][key]
end
--  vararg  UserData.set(memberId, key, value)
--    Sets value as key to the UserData. Does not immediately sync to the flat database.
function declare__set(self, memberId, key, value)
  Debugging.checkSelf(
    self,
    root
  )
  Debugging.checkType(
    memberId,
    "string",
    1
  )
  local __type_key = type(key)
  assert(__type_key ~= "nil", "bad argument #1 (key cannot be nil, got ".. __type_key ..")")
  ----  set and return results
  local userData = getUserData(memberId)
  userData[2][key] = value
  return value
end
------  FINALIZE
----  connect to events
Client:on(
  "presenceUpdate",
  function(updatingMember)
    updateUserData(updatingMember)
  end
)
----  fire ready
ready()
----  save loop
while true do
  wait(10)
  --  go through every member and run updateUserData
  for memberId, member in next, GUILD.members[2] do
    updateUserData(member)
  end
end
