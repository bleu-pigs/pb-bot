--  Modules/LuaSignal.lua
----  localize environment
local require = require
local _G = _G
----  localize library
local PigBot = PigBot
------  MODULE API
----  LuaSignal ROOT table
local LuaSignal = {
  Signals = {}
}
----  LuaSignal FUNCTIONS
--  Signal  LuaSignal.new()
--    Creates a new Signal object, replicating the functionality of a RBXScriptSignal
local Signals = LuaSignal.Signals
function LuaSignal.new()
  ----  variables
  local newLuaSignal = {}
  local connections, waitingThreads = {}, setmetatable(
    {},
    {
      __mode = "v"
    }
  )
  ----  newLuaSignal FUNCTIONS
  --  void  newLuaSignal:fire(...)
  --    Resumes all waiting threads and executes all listeners
  function newLuaSignal:fire(...)
    local thread
    --  resume all waiting threads
    for key = 1, #waitingThreads do
      if #waitingThreads == 0 then break end
      print(#waitingThreads, waitingThreads[1][2])
      print(debug.traceback())
      --  get our thread
      thread = table.remove(waitingThreads, 1)[1]
      --  resume the thread
      coroutine.resume(thread, ...)
    end
    --  execute all listeners
    for connection in next, connections do
      --  create a dedicated thread for the callback
      -- PigBot.Scheduler.spawn(
      --   connection._callback,
      --   ...
      -- )
      -- if spawn == nil then
        coroutine.resume(
          coroutine.create(
            connection._callback
          ),
          ...
        )
      -- else
      --   spawn(
      --     connection._callback,
      --     ...
      --   )
      -- end
    end
  end
  ----  newLuaSignal.Event
  ----    Represents the Event API
  local Event = {}
  ----  newLuaSignal.Event FUNCTIONS
  --  connection  Event:connect(function callback)
  --    Connects callback as a listener, and returns a connection object
  function Event:connect(callback)
    local __type_callback = type(callback)
    assert(__type_callback == "function", "bad argument #1 (expected function, got ".. __type_callback ..")")
    ----  variables
    local connection = {
      _callback = callback,
      connected = true
    }
    ----  connection FUNCTIONS
    --  void  connection:disconnect()
    --    Removes the listener from the Signal
    function connection:disconnect()
      connections[connection] = nil
      connection.connected = false
      connection._callback = nil
    end
    --  register our connection
    connections[connection] = true
    --  return connection
    return connection
  end
  --  ...  Event:wait()
  --    Yields the running thread until the Event fires
  function Event:wait()
    waitingThreads[#waitingThreads + 1] = {coroutine.running(), debug.traceback()}
    return coroutine.yield()
  end
  ----  newLuaSignal.Event SET
  newLuaSignal.Event = Event
  ----  newLuaSignal FUNCTIONS RESUME
  -- void  newLuaSignal:destroy()
  --    Destroys Signal and all of its connections
  function newLuaSignal:destroy()
    --  disconnect all connections
    for connection in next, connections do
      connection:disconnect()
    end
    --  remove all variables
    waitingThreads = nil
    connections = nil
    Event.connect = nil
    Event.wait = nil
    Event = nil
    newLuaSignal.Event = nil
    newLuaSignal.fire = nil
    newLuaSignal.wait = nil
    newLuaSignal.destroy = nil
    newLuaSignal = nil
  end
  ----  RETURN newLuaSignal
  Signals[#Signals + 1] = newLuaSignal
  return newLuaSignal
end
------  FINALIZATION
PigBot.LuaSignal = LuaSignal
return LuaSignal
