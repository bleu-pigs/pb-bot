--  Modules/NewMember.lua
local JSON = Utilities.JSON
local httpRequest = require("coro-http").request
----  MODULE BASE
--  constants
--  emoji constants
local EMOJIS = {
  THUMBS_UP = "👍",
  THUMBS_DOWN = "👎"
}
local EMOJIS_LOOKUP = {}
for key, value in next, EMOJIS do
  EMOJIS_LOOKUP[value] = key
end
--  USER_RESPONSES
--    contains all responses we expect from user
local USER_RESPONSES = {
    TOS_ACCEPT = "i agree",
    TOS_DECLINE = "i disagree",
    KNOWLEDGE_ACCEPT = "take test",
    KNOWLEDGE_DECLINE = "decline test",
    VERIFICATION_YES = "yes",
    VERIFICATION_NO = "no"
}
--  BOT_MESSAGES
--    contains all responses give from this module
--    updated for the restructure
local BOT_MESSAGES = {
    JOINING_WELCOME = "**Thanks for showing interest in the Bleu Pigs ScriptBuilder community hangout!**\n\nWARNING: This bot is currently under development, and is running on a development build. This means the bot can break or crash at any random moment with no notice. I have implemented a UserData persistence system which does a pretty good job at keeping persisted data, however it only saves every 10 seconds or when you change to a status that isn't \"online\". This means PigBot may not always capture your most recent interactions, and may make you retake portions over again. If you understand this, simply proceed with the provided instructions in the next message. If not, you may leave the Server with no consequence.",
    JOINING_WELCOME_BACK = "**Welcome back to Bleu Pigs!**\n\nDon't worry, %s, PigBot's got you covered! UserData persistence has restored your roles and kept your user preferences. Everything should be just as you left :)",
    JOINING_RESUMING_TEST = "**Looks like the joining process was interupted!**\n\nDon't worry, PigBot's got you covered! UserData persistence will send you right back to where you were in the joining process! :)\n\nResuming at step %s",
    JOINING_RETEST_REQUIRED = "**Welcome back to Bleu Pigs!**\n\nLooks like you've been gone a while, %s! We'll need you to retake the joining process to ensure you're still fit for the community.",
    TOS_READ_AND_ACCEPT = "**Please read our rules, it's important!!**\n\nBefore you can continue in the joining process, we need you to read the <#".. BotConfig.Management.GuildGuidebook .."> and agree to our Terms of Service located at ".. BotConfig.Joining.TermsOfServiceUrl .."\n\nIf you agree, please reply with \"".. USER_RESPONSES.TOS_ACCEPT .."\". If you disagree, please reply with \"".. USER_RESPONSES.TOS_DECLINE .."\" or do not respond for 5 minutes",
    TOS_DECLINED = "**Access declined :(**\n\nUnforunately, you must accept our ToS and guidebook or you cannot continue in the joining process. If you wish to continue, please rejoin and agree at any time.".. BotConfig.Joining.DiscordInvite,
    TOS_UNEXPECTED_RESPONSE = "**Sorry, I don't understand gibberish**.\n\nPlease respond with either \"".. USER_RESPONSES.TOS_ACCEPT .."\" or \"".. USER_RESPONSES.TOS_DECLINE .."\"",
    VERIFICATION_CHECKING_STATUS = "**Are you verified?**\n\nIn order to ensure you are who you claim, Bleu Pigs requires all members to be verified with the third party service <@298796807323123712>. I'm currently checking to see if you are already verified or need to verify. Please stand by ...",
    VERIFICATION_REQUIRED = "**VERIFICATION REQUIRED!!**\n\nBefore you can continue any further, you must verify with RoVer. To get started, visit https://verify.eryn.io",
    VERIFICATION_IS_THIS_YOU = "**Is this you?**\n\nPigBot has identified you as **%s**, and wants to make sure this is you.\n\nTo confirm this is you, reply with \"".. USER_RESPONSES.VERIFICATION_YES .."\", otherwise reply with \"".. USER_RESPONSES.VERIFICATION_NO .."\" to decline and try again.",
    VERIFICATION_RETRY = "**Looks like you ran into an issue when verifying!**\n\nIf this is your first time, you might be having issues with having a different discord account signed into your browser. Visit https://eryn.io/RoVer/, scroll down to \"Frequently Asked Questions and Common Issues\" and check out \"User Verification Issues\" to solve common first time use issues.\n\nOnce you're confident you've logged into the right accounts, visit https://verify.eryn.io and perform the whole process over again. The bot will give you a total of 2 minutes and 30 seconds before checking your verification status again.",
    KNOWLEDGE_TEST_REQUIRED = "**Basic Lua knowledge test required for entry**\n\nBefore you can continue in the joining process, you must pass a basic Lua knowledge test with a score of at least ".. BotConfig.Joining.RequiredTestGrade ..", otherwise you'll be kicked and you can rejoin at any time to resume where you left off in the joining process.\n\n**WARNING:**\n- If you fail the test, you will be banned for ".. BotConfig.Joining.DeclinedBanTime .." days to restudy.\n- You have a total of ".. BotConfig.Joining.QuestionsRequired * BotConfig.Joining.TimeAllocatedPerQuestion .." seconds to answer all the questions. If you fail to answer the questions in time, you will be banned for ".. BotConfig.Joining.DeclinedBanTime .." days.\nAfter the ban is lifted, you are free to rejoin and try again as many times as you like.\n\nWhen you are ready to take the test, reply with \"".. USER_RESPONSES.KNOWLEDGE_ACCEPT .."\" to begin, or \"".. USER_RESPONSES.KNOWLEDGE_DECLINE .."\" to decline the test and leave the server.",
    KNOWLEDGE_UNEXPECTED_RESPONSE = "**Sorry, I don't understand gibberish.**\n\nPlease respond with either \"".. USER_RESPONSES.KNOWLEDGE_ACCEPT .."\" or \"".. USER_RESPONSES.KNOWLEDGE_DECLINE .."\"",
    KNOWLEDGE_UNEXPECTED_ANSWER_RESPONSE = "**Sorry, I don't understeand gibberish.**\n\nYou must use numbers between 1 and 4. Do not include anything else but the number.",
    KNOWLEDGE_TEST_DECLINED = "**Access declined :(**\n\nUnforunately, you must do the basic Lua knowledge test before you can continue to the next part of the joining process. If you wish to continue, PigBot has saved where you are at and will continue where you left off if you decide to try again. Simply rejoin the server at any time and we'll get you started.".. BotConfig.Joining.DiscordInvite,
    KNOWLEDGE_TEST_FAILED = "**Access declined: you failed to pass the test.**\n\nUnforunately, you must be able to pass a basic Lua knowledge test before you can continue in the joining process. Please come back within ".. BotConfig.Joining.DeclinedBanTime .." days after studying to try again.\nReceived Grade: %s\nRequired Grade: ".. BotConfig.Joining.RequiredTestGrade .."\n".. BotConfig.Joining.DiscordInvite,
    KNOWLEDGE_CORRECT_ANSWER = "**CORRECT ANSWER!**",
    KNOWLEDGE_INCORRECT_ANSWER = "**INCORRECT ANSWER!**",
    KNOWLEDGE_TEST_RAN_OUT = "**There's no more questions to give!**\n\nUnforunately, it looks like PigBot has ran out of new questions to give you. You must come back within ".. BotConfig.Joining.DeclinedBanTime .." to try again.\n".. BotConfig.Joining.DiscordInvite,
    GROUPCHECK_CHECKING = "**Checking group membership ...**\n\nPlease hold on while we check.",
    GROUPCHECK_JOIN_REQUIRED = "**Join the Bleu Pigs ROBLOX group!**\n\nPart of requirements is you must be a member of our ROBLOX Group, \"Bleu Pigs\". By being a member, your role will sync with your Discord, and if approved, will grant you access to our community ran ScriptBuilder and various features in the group.\nhttps://www.roblox.com/groups/group.aspx?gid=".. BotConfig.Management.ManagedGroup,
    COMMUNITYAPPROVAL_REQUIRED = "**Community approval required**\n\nCongratulations, you've passed our requirements! You must now be approved by at least ".. BotConfig.Joining.ApprovalPercentage .."% of our community before you can be let in. We've just sent out a notification in our approvals channel, and pinged members subscribed to our Approvers role.\n\nYou have a total of ".. BotConfig.Joining.VotingDays .." days before your vote ends. If you do not get at least ".. BotConfig.Joining.ApprovalPercentage .."%, you will be banned from the server for ".. BotConfig.Joining.DeclinedBanTime .." days. After this time frame, you may rejoin and try to get approval from the community again.",
    COMMUNITYAPPROVAL_JOIN_REQUEST = "<@&".. BotConfig.Management.ApproversRole ..">\n%s would like to join the Bleu Pigs!\nReceived test grade: %s\nhttps://www.roblox.com/users/%s/profile/",
    COMMUNITYAPPROVAL_USER_LEFT_EDIT_VOTE = "Looks like %s left on us! The vote has been cancelled.",
    COMMUNITYAPPROVAL_USER_LEFT_EDIT_DM = "@everyone\n**Looks like you left on us during the voting process!**\n\nUnforunately, you must stay in the Server if you wish to be approved. Don't worry, we saved where you were at in the joining process. You can retry at any time simply by rejoining our server, and we'll continue where we left off.\n".. BotConfig.Joining.DiscordInvite,
    COMMUNITYAPPROVAL_DECLINED_EDIT_VOTE = "**%s** has been declined entrance from this community. They will be permitted to try again within ".. BotConfig.Joining.DeclinedBanTime .." days.\n\nReceived rating: %s\nRequired rating: ".. BotConfig.Joining.ApprovalPercentage,
    COMMUNITYAPPROVAL_EMOJI_ADDED_ALERT ="<@%s>,\nLooks like you're attempting to add an Emoji to the voting channel! Unforunately, to prevent confusion, we cannot allow this. If you attempting to vote, voting is easy! Simply click the ".. EMOJIS.THUMBS_UP .." to show your sign of approval, or the ".. EMOJIS.THUMBS_DOWN .." so show your sign of disapproval.",
    ENTRY_ACCEPTED = "**ACCEPTED! Welcome, new member of Bleu Pigs! :)**\n\nCongratulations! You're now an official member of Bleu Pigs. You now have basic member access to the Bleu Pigs discord and our community ScriptBuilder.\n\n**Please be sure to read <#".. BotConfig.Management.GuildGuidebook ..">, as it will tell you how this community operates. You have been warned.**",
    ENTRY_DECLINED =  "**Unforunately, you've been declined by the community.**\n\nWe're sorry, but you've been declined for entry by our community. Per our rules, you have been banned for ".. BotConfig.Joining.DeclinedBanTime .." as a cool-down period. You may rejoin after your ban and try again.\n\nApproval received: %s\nApproval required: ".. BotConfig.Joining.ApprovalPercentage .."\n".. BotConfig.Joining.DiscordInvite,
    ERROR_USER_TIMED_OUT = "**Looks like you timed out while attempting to join Bleu Pigs!**\n\nDon't worry, we saved where you were at in the process. When you're ready to continue, come back and we'll continue where you left off.".. BotConfig.Joining.DiscordInvite,
    ERROR_LEFT_MID_PROCESS = "**Looks like you left the server while attempting to join Bleu Pigs!**\n\nUnforunately, you need to stay in the server in order to continue joining. If you wish to come back, PigBot has saved where you were in the process for the next ".. BotConfig.Joining.MustRetakeJoiningTestAfterSetDays .." days. Come back anytime within then and we'll continue where you left off.\n".. BotConfig.Joining.DiscordInvite,
    ERROR_SOMETHING_IS_FUNKY = "**Something's not right here ...**\nLooks like something funky is going on with the http request. I'll retry a bit before giving up.",
    ERROR_BAD_SHUTDOWN = "**Something bad happened with PigBot!**\n\nLooks like PigBot was suddenly terminated without warning. This could have been related to a crash or because the bot was restarted.\n\nInformation from Developer:\n%s".. BotConfig.Joining.DiscordInvite,
    MESSAGERECEIVED_SPEAK_IN_RECEIVINGCHANNEL = "%s, you're speaking in the wrong channel. I'm set up to listen to your commands in <#%s>!"
}
local USER_FAILED_RESPONSE = {
  ERROR_USER_TIMED_OUT = "user timed out",
  ERROR_LEFT_MID_PROCESS = "user left server",
  ERROR_COULDNT_FIND_BOOLEAN = "couldnt find boolean in group join verification",
  TOS_DECLINED = "user declined terms of service",
  KNOWLEDGE_TEST_DECLINED = "user declined to take basic Lua knowledge test",
  KNOWLEDGE_TEST_FAILED = "user failed test (".. BotConfig.Joining.RequiredTestGrade .."% mininal required)",
  KNOWLEDGE_TEST_RAN_OUT = "user ran out of new questions to answer",
  ENTRY_DECLINED = "user declined for entry"
}
--  discord constants
local GUILD = DiscordExtensions.GUILD
local CHANNELS = {
  ALERTS = GUILD:getChannel(BotConfig.Notifications.AlertsChannel),
  APPROVALS = GUILD:getChannel(BotConfig.Joining.ApprovalsChannel)
}
--  conversions constants
local STRING_TO_NUMBER = {
  ["1"] = 1,
  ["2"] = 2,
  ["3"] = 3,
  ["4"] = 4
}
-- for key, value in next, GUILD.roles[2] do
--   print(key, value.name)
-- end
--  http configurations
local ROVER_OPTIONS = {
  host = "https://verify.eryn.io",
  path = "/api/user/%s",
  method = "GET"
}
local ROBLOX_GROUP_OPTIONS = {
  host = "https://assetgame.roblox.com",
  path = "/Game/LuaWebService/HandleSocialRequest.ashx?method=IsInGroup&playerid=%s&groupid=%s",
  method = "GET"
}
--  lua test constants
local LUA_TEST = require("LuaKnowledgeTest")
----  MODULE API
local sendHttpRequest = Utilities.sendHttpRequest
local newCounter = Utilities.newCounter
--  boolean, string  performTOSCheck(member)
--    Has the user accept the Terms of Service
local function performTOSCheck(member)
  Debugging.write(
    "starting performTOSCheck for %s",
    member.username
  )
  ----  variables
  local sentNotification = DiscordExtensions:sendNotificationToMember(
    member,
    BOT_MESSAGES.TOS_READ_AND_ACCEPT,
    true
  )
  Debugging.checkType(
    sentNotification,
    "table",
    1
  )
  local counter = newCounter(300)
  --  go ahead and wait for the user to respond
  local responseReceived, response = DiscordExtensions:getSpecificReplyFromMember(
    member,
    counter,
    {
      USER_RESPONSES.TOS_ACCEPT,
      USER_RESPONSES.TOS_DECLINE
    }
  )
  Debugging.checkType(
    responseReceived,
    "boolean",
    2
  )
  Debugging.checkType(
    response,
    {
      "string",
      "nil"
    },
    3
  )
  --  go ahead and exit now if we didnt receive a response
  if responseReceived == false then
    return responseReceived, response, sentNotification
  end
  --  otherwise, perform logic check
  if response == USER_RESPONSES.TOS_ACCEPT then
    return true
  end
  --  if we got here, user declined Terms of Service.
  sentNotification:delete()
  DiscordExtensions:sendNotificationToMember(
    member,
    BOT_MESSAGES.TOS_DECLINED,
    true
  )
  return false, USER_FAILED_RESPONSE.TOS_DECLINED
end
--  boolean, string  performUserVerification(member)
--    Checks to see if the member is verified. Integrates with RoVer.
local function performUserVerification(member)
  Debugging.write(
    "starting performUserVerification for %s",
    member.username
  )
  ----  variables
  local sentNotification
  local counter = newCounter(6)
  local responseTimeoutCounter = newCounter(300)
  local hasReceivedAlert, somethingsActingFunky = false, false
  --  first, lets see if we already have the userId before doing anything else
  local robloxUserId = UserData:get(
    member.id,
    "robloxUserId"
  )
  Debugging.checkType(
    robloxUserId,
    {
      "string",
      "nil"
    },
    1
  )
  if robloxUserId ~= nil then
    --  we got our id! end now
    return true, robloxUserId
  end
  --  check to see if the user has been verified
  local responseHeaders, httpResponse
  --  check our response
  while true do
    sentNotification = DiscordExtensions:sendNotificationToMember(
      member,
      BOT_MESSAGES.VERIFICATION_CHECKING_STATUS,
      true
    )
    Debugging.checkType(
      sentNotification,
      "table",
      1
    )
    --  did they leave on us? make sure the member is still part of the server
    if DiscordExtensions:isMemberStillInGuild(member.id) == false then
      --  looks like they left on us, gotta terminate.
      return false, USER_FAILED_RESPONSE.ERROR_LEFT_MID_PROCESS, sentNotification
    end
    --  perform the request
    responseHeaders, httpResponse = sendHttpRequest(
      ROVER_OPTIONS.method,
      ROVER_OPTIONS.host .. ROVER_OPTIONS.path:format(member.id)
    )
    Debugging.checkType(
      responseHeaders,
      "table",
      1
    )
    Debugging.checkType(
      httpResponse,
      {
        "table",
        "string"
      },
      1
    )
    --  check the response
    if responseHeaders.code == 404 and httpResponse.error == "User not found." then
      --  user has not verified with RoVer, require them to do so.
      --  check to see if we've already sent an alert in the past.
      if hasReceivedAlert ~= true then
        --  we havent sent an alert to the user, doing so now.
        sentNotification:setContent(BOT_MESSAGES.VERIFICATION_REQUIRED)
        hasReceivedAlert = true
      end
    elseif responseHeaders.code == 200 and httpResponse.status == "ok" then
      --  user has verified with RoVer, check to make sure it its them first.
      sentNotification:setContent(
        BOT_MESSAGES.VERIFICATION_IS_THIS_YOU:format(
          httpResponse.robloxUsername
        )
      )
      --  try to get our response
      local receivedResponse, userResponse = DiscordExtensions:getSpecificReplyFromMember(
        member,
        newCounter(60),
        {
          USER_RESPONSES.VERIFICATION_YES,
          USER_RESPONSES.VERIFICATION_NO
        }
      )
      Debugging.checkType(
        receivedResponse,
        "boolean"
      )
      Debugging.checkType(
        userResponse,
        "string"
      )
      --  if they didnt respond to us, go ahead and boogy out.
      if receivedResponse == false then
        return receivedResponse, userResponse, sentNotification
      end
      --  otherwise, check our response
      sentNotification:delete()
      if userResponse == USER_RESPONSES.VERIFICATION_YES then
        --  user is verified, update UserData
        robloxUserId = UserData:set(
          member.id,
          "robloxUserId",
          httpResponse.robloxId ..""
        )
        return true, robloxUserId
      end
      --  we got here, lets try again
      sentNotification = DiscordExtensions:sendNotificationToMember(
        member,
        BOT_MESSAGES.VERIFICATION_RETRY,
        true
      )
    else
      --  something funky is going on here
      if somethingsActingFunky == false then
        somethingsActingFunky = true
        sentNotification:setContent(BOT_MESSAGES.ERROR_SOMETHING_IS_FUNKY)
        --  debug print
      end
    end
    --  if we got here, lets check to see how long weve been running
    if counter() == false then
      --  weve been running too long, terminate.
      sentNotification = DiscordExtensions:sendNotificationToMember(
        member,
        BOT_MESSAGES.ERROR_USER_TIMED_OUT,
        true
      )
      return false, USER_FAILED_RESPONSE.ERROR_USER_TIMED_OUT, sentNotification
    end
    --  wait for about 2.5 minutes
    wait(15)
    sentNotification:delete()
  end
end
--  boolean, string  performLuaTest(member)
--    Has the user perform a Lua test, seeing if they qualify for Bleu Pigs membership.
local function performLuaTest(member)
  Debugging.write(
    "starting performLuaTest for %s",
    member.username
  )
  ----  variables
  --  get userdata
  local joiningProcessData = UserData:get(
    member.id,
    "joiningProcessData"
  )
  Debugging.checkType(
    joiningProcessData,
    "table"
  )
  Debugging.write("got joiningProcessData")
  local previouslyAnswered = joiningProcessData.previouslyAnswered
  Debugging.checkType(
    previouslyAnswered,
    {
      "table",
      "nil"
    }
  )
  if previouslyAnswered == nil then
    previouslyAnswered = {}
    joiningProcessData.previouslyAnswered = previouslyAnswered
  end
  Debugging.write("got previouslyAnswered")
  --  logic variables
  local correct, incorrect, answered, grade = 0, 0, joiningProcessData.answered
  Debugging.checkType(
    answered,
    {
      "number",
      "nil"
    }
  )
  -- if not correct then
  --   correct = 0
  --   joiningProcessData.correct = correct
  -- end
  -- if not incorrect then
  --   incorrect = incorrect
  --   joiningProcessData.incorrect = incorrect
  -- end
  if answered == nil then
    answered = 0
    joiningProcessData.answered = answered
  end
  Debugging.write("got answered")
  local question, receivedMessage, messageToSend, answer, answers, answersLookup
  local counter = newCounter(120)
  local sentNotification = DiscordExtensions:sendNotificationToMember(
    member,
    BOT_MESSAGES.KNOWLEDGE_TEST_REQUIRED,
    true
  )
  ------  LOGIC BEGIN
  ----  wait for the user to agree to take the test
  Debugging.write("waiting for response")
  local receivedResponse, response = DiscordExtensions:getSpecificReplyFromMember(
    member,
    counter,
    {
      USER_RESPONSES.KNOWLEDGE_ACCEPT,
      USER_RESPONSES.KNOWLEDGE_DECLINE
    }
  )
  --  go ahead and boogy out if we didnt get a response
  if receivedResponse == false then
    return receivedResponse, response, sentNotification
  end
  --  otherwise, lets check the responses
  if response == USER_RESPONSES.KNOWLEDGE_ACCEPT then
    Debugging.write("user accepted, setting up test")
    --  user is ready to take their test, lets set them up.
    counter = newCounter(BotConfig.Joining.QuestionsRequired * BotConfig.Joining.TimeAllocatedPerQuestion)
    --  begin test
    sentNotification:delete()
    Debugging.write("starting test")
    for _ = 1, BotConfig.Joining.QuestionsRequired do
      ----  gotta get our question first
      Debugging.write("getting question ...")
      question = nil
      while question == nil do
        --  do we have enough questions to give? lets check
        if answered >= #LUA_TEST then
          Debugging.write(
            "%s ran out of questions, terminating",
            member.username
          )
          --  we've ran out of questions to give, terminate.
          joiningProcessData.ranOutOfQuestions = true
          sentNotification:delete()
          DiscordExtensions:sendNotificationToMember(
            member,
            BOT_MESSAGES.KNOWLEDGE_TEST_RAN_OUT,
            true
          )
          return false, USER_FAILED_RESPONSE.KNOWLEDGE_TEST_RAN_OUT
        end
        question = LUA_TEST[math.random(1, #LUA_TEST)]
        if previouslyAnswered[question[1]] == nil then
          Debugging.write("question selected")
          break
        end
        wait(0)
        Debugging.write(answered .."\n".. #LUA_TEST .."\n".. question[1])
        question = nil
        wait(0)
      end
      Debugging.write("preparing question")
      ----  prepare the message to send
      messageToSend = question[1] .."\n"
      --  try to randomly place the questions in order
      Debugging.write("randomizing answers ...")
      answers, answersLookup = {}, {}
      for key = 1, 4 do
        local selectedAnswer
        while selectedAnswer == nil do
          selectedAnswer = question[3][math.random(1, 4)]
          if answersLookup[selectedAnswer] == nil then
            answers[#answers + 1] = selectedAnswer
            answersLookup[selectedAnswer] = key
            break
          end
          wait(0)
          selectedAnswer = nil
        end
      end
      Debugging.write("randomize complete")
      --  generate answers
      for number = 1, 4 do
        messageToSend = messageToSend .. number ..") ".. answers[number] .."\n"
      end
      ----  send the message to the user
      sentNotification = DiscordExtensions:sendNotificationToMember(
        member,
        messageToSend,
        true
      )
      ----  wait for our user to respond
      receivedResponse, response = DiscordExtensions:getSpecificReplyFromMember(
        member,
        counter,
        {
          "1",
          "2",
          "3",
          "4"
        }
      )
      --  go ahead and boogy out if we didnt get a response back
      if receivedResponse == false then
        return receivedResponse, response, sentNotification
      end
      --  otherwise, process our response
      response = answers[STRING_TO_NUMBER[response]]
      if response == question[2] then
        correct = correct + 1
        sentNotification:setContent(BOT_MESSAGES.KNOWLEDGE_CORRECT_ANSWER)
      else
        incorrect = incorrect + 1
        sentNotification:setContent(BOT_MESSAGES.KNOWLEDGE_INCORRECT_ANSWER)
      end
      previouslyAnswered[question[1]] = true
      answered = answered + 1
    end
    --  test is over! time to grade their test
    grade = math.ceil((correct/BotConfig.Joining.QuestionsRequired) * 100)
    joiningProcessData.receivedGrade = grade
    joiningProcessData = UserData:set(
      member.id,
      "joiningProcessData",
      joiningProcessData
    )
    if grade >= BotConfig.Joining.RequiredTestGrade then
      --  user passed the test!
      return true
    else
      --  user failed the test
      DiscordExtensions:sendNotificationToMember(
        member,
        BOT_MESSAGES.KNOWLEDGE_TEST_FAILED:format(grade),
        true
      )
      return false, USER_FAILED_RESPONSE.KNOWLEDGE_TEST_FAILED
    end
  end
  --  if we got here, we declined the test.
  sentNotification:delete()
  DiscordExtensions:sendNotificationToMember(
    member,
    BOT_MESSAGES.KNOWLEDGE_TEST_DECLINED,
    true
  )
  return false, USER_FAILED_RESPONSE.KNOWLEDGE_TEST_DECLINED
end
--  boolean, string  performRequireGroup(member, messageReceived, robloxId)
--    Requires the user to join our ROBLOX group before continuing
local function performRequireGroup(member, robloxId)
  Debugging.write(
    "starting performRequireGroup for %s",
     member.username
   )
  ----  variables
  local sentNotification = DiscordExtensions:sendNotificationToMember(
    member,
    BOT_MESSAGES.GROUPCHECK_CHECKING,
    true
  )
  local responseHeaders, hasUserJoined
  local counter = newCounter(60)
  local somethingsActingFunky, alertedNotInGroup = false, false
  ----  begin verification checks
  while true do
    --  did they leave on us? make sure the member is still part of the server
    if DiscordExtensions:isMemberStillInGuild(member.id) == false then
      --  looks like they left on us, gotta terminate.
      sentNotification:setContent(BOT_MESSAGES.ERROR_LEFT_MID_PROCESS)
      return false, USER_FAILED_RESPONSE.ERROR_LEFT_MID_PROCESS, sentNotification
    end
    --  send the http request
    responseHeaders, hasUserJoined = sendHttpRequest(
      ROBLOX_GROUP_OPTIONS.method,
      ROBLOX_GROUP_OPTIONS.host .. ROBLOX_GROUP_OPTIONS.path:format(
        robloxId,
        BotConfig.Management.ManagedGroup
      )
    )
    Debugging.checkType(
      responseHeaders,
      {
        "boolean",
        "table"
      }
    )
    Debugging.checkType(
      hasUserJoined,
      {
        "string",
        "nil"
      }
    )
    Debugging.write(
      tostring(responseHeaders),
      tostring(hasUserJoined)
    )
    --  break down userHasJoined
    if hasUserJoined:find("boolean\">false") then
      Debugging.write("user is not in group")
      if alertedNotInGroup == false then
        sentNotification:setContent(BOT_MESSAGES.GROUPCHECK_JOIN_REQUIRED)
        alertedNotInGroup = true
      end
    elseif hasUserJoined:find("boolean\">true") then
      Debugging.write("user is in group")
      sentNotification:delete()
      return true
    else
      --  somethings going wrong here. debug as much info as possible
      if somethingsActingFunky ~= true then
        somethingsActingFunky = true
        sentNotification:setContent(BOT_MESSAGES.ERROR_SOMETHING_IS_FUNKY)
        --  debug print
        Debugging.write("printing responseHeaders")
        for key, value in next, responseHeaders do
          Debugging.write(key, value)
        end
        Debugging.write("----\n------- END OF RESPONSE HEADERS\n----")
        Debugging.write("printing response")
        if type(hasUserJoined) == "table" then
          for key, value in next, response do
            Debugging.write(key, value)
          end
        else
          Debugging.write(hasUserJoined)
        end
        Debugging.write("----\n------- END OF RESPONSE\n----")
        Debugging.write(member.id)
        for key, value in next, ROBLOX_GROUP_OPTIONS do
          Debugging.write(key, value)
        end
        Debugging.write(
          ROBLOX_GROUP_OPTIONS.host .. ROBLOX_GROUP_OPTIONS.path:format(
            robloxId,
            BotConfig.Management.ManagedGroup
          )
        )
      end
      --  how long have weve been running?
      if counter() == false then
        --  weve been at it for a while, lets call quits.
        return false, USER_FAILED_RESPONSE.ERROR_USER_TIMED_OUT, sentNotification
      end
      wait(1)
    end
    --  how long have weve been at this? lets see if the user timed out
    if counter() == false then
      --  weve been running too long, terminate.
      return false, USER_FAILED_RESPONSE.ERROR_USER_TIMED_OUT, sentNotification
    end
    wait(90)
  end
end
--  boolean, string  performCommunityCheck(member, messageReceived)
--    User has passed all checks! Posts a notification in the BotConfig.Joining.ApprovalsChannel
--    and alerts users with the BotConfig.Management.ApproversRole
local function performCommunityCheck(member, robloxId)
  Debugging.write(
    "starting performCommunityCheck for %s",
    member.username
  )
  ----  variables
  local sentNotification = DiscordExtensions:sendNotificationToMember(
    member,
    BOT_MESSAGES.COMMUNITYAPPROVAL_REQUIRED,
    true
  )
  local approvalMessage
  local joiningProcessData = UserData:get(
    member.id,
    "joiningProcessData"
  )
  --  voting
  local approved, denied, rating, timeRemaining = 0, 0, 0, joiningProcessData.voting_timeRemaining
  local usersWhoveVoted = {
    [Client.user.id] = true
  }
  local totalMembers = 0 + GUILD.members[1]
  -- if approved == nil then
  --   approved = 0
  --   takingTestData.voting_approved = approved
  -- end
  -- if denied == nil then
  --   denied = 0
  --   takingTestData.voting_denied = denied
  -- end
  -- if rating == nil then
  --   rating = 0
  --   takingTestData.voting_rating = rating
  -- end
  if timeRemaining == nil then
    timeRemaining = BotConfig.Joining.VotingHours * 3600
    joiningProcessData.voting_timeRemaining = timeRemaining
  end
  --  listeners
  --  add our reactions, and wait for them to be created
  local isBotRemoval = false
  local approvedReaction, declinedReaction, emojiAddedListener, emojiRemovedListener
  emojiAddedListener = DiscordExtensions.EVENTS.reactionAdd:connect(
    function(newEmoji, fromUserId)
      if type(newEmoji) ~= "table" then return end
      --  first, lets see if this is on our approval message
      if newEmoji.message.id == approvalMessage.id then
        --  new emoji attached to our message! is it approved?
        if EMOJIS_LOOKUP[newEmoji.emojiName] == nil then
          --  uh oh, looks like someone is adding additional reactions! lets
          --  tell them how to use the system and delete it.
          isBotRemoval = true
          newEmoji:delete(fromUserId)
          local hasReceivedAlert = UserData:get(
            fromUserId,
            "performCommunityCheck_hasReceivedAlert"
          )
          if hasReceivedAlert ~= true then
            --  user has not received an alert in the past.
            hasReceivedAlert = UserData:set(
              fromUserId,
              "performCommunityCheck_hasReceivedAlert",
              true
            )
            DiscordExtensions:sendNotification(
              BOT_MESSAGES.COMMUNITYAPPROVAL_EMOJI_ADDED_ALERT:format(fromUserId)
            )
          end
        elseif approvedReaction ~= nil and declinedReaction ~= nil then
          --  someones updated one of our emojis! lets keep track
          --  and prevent double voters
          if newEmoji.emojiName == EMOJIS.THUMBS_UP then
            if usersWhoveVoted[fromUserId] == true then
              --  user has already voted, remove.
              isBotRemoval = true
              newEmoji:delete(fromUserId)
            else
              usersWhoveVoted[fromUserId] = true
            end
          end
          if newEmoji.emojiName == EMOJIS.THUMBS_DOWN then
            if usersWhoveVoted[fromUserId] == true then
              --  user has already voted, remove
              isBotRemoval = true
              newEmoji:delete(fromUserId)
            else
              usersWhoveVoted[fromUserId] = true
              totalMembers = totalMembers + 1
            end
          end
        end
      end
    end
  )
  emojiRemovedListener = DiscordExtensions.EVENTS.reactionRemove:connect(
    function(removingEmoji, fromUserId)
      if isBotRemoval == true then
        isBotRemoval = false
        return
      end
      if removingEmoji.message.id == approvalMessage.id then
        --  remove usersWhoveVoted
        usersWhoveVoted[fromUserId] = false
        --  update totalMembers
        if removingEmoji.emojiName == EMOJIS.THUMBS_DOWN then
          totalMembers = totalMembers - 1
        end
        return
      end
    end
  )
  ----  first, lets check to see if there was a previous vote going on
  approvalMessage = joiningProcessData.joining_approvalMessage
  if approvalMessage ~= nil then
    approvalMessage = DiscordExtensions.USABLE_CHANNELS.APPROVALS:getMessage(approvalMessage)
    if approvalMessage ~= nil then
      --  now, if its a cancelled vote, lets restart it
      if approvalMessage.content:match(member.id) == nil then
        approvalMessage:setContent(
          BOT_MESSAGES.COMMUNITYAPPROVAL_JOIN_REQUEST:format(
            member.user.mentionString,
            robloxId
          )
        )
      end
      --  lets just resuse the message instead of having to ping everyone again
      usersWhoveVoted = joiningProcessData.usersWhoveVoted
      if usersWhoveVoted == nil then
        usersWhoveVoted = {
          [Client.user.id] = true
        }
        joiningProcessData.usersWhoveVoted = usersWhoveVoted
      end
      approvedReaction = approvalMessage.reactions[2][EMOJIS.THUMBS_UP]
      declinedReaction = approvalMessage.reactions[2][EMOJIS.THUMBS_DOWN]
      totalMembers = totalMembers + declinedReaction:getUsers()[1]
    end
  end
  if approvalMessage == nil then
    --  looks like we gotta send a notification and wait for the emojis
    approvalMessage = DiscordExtensions.USABLE_CHANNELS.APPROVALS:send(
      BOT_MESSAGES.COMMUNITYAPPROVAL_JOIN_REQUEST:format(
        member.mentionString,
        joiningProcessData.receivedGrade,
        robloxId
      )
    )
    approvalMessage:addReaction(EMOJIS.THUMBS_UP)
    approvalMessage:addReaction(EMOJIS.THUMBS_DOWN)
    approvedReaction = DiscordExtensions:waitForReaction(approvalMessage, EMOJIS.THUMBS_UP)
    declinedReaction = DiscordExtensions:waitForReaction(approvalMessage, EMOJIS.THUMBS_DOWN)
  end
  usersWhoveVoted[Client.user.id] = true
  joiningProcessData.joining_approvalMessage = approvalMessage.id ..""
  ----  LOGIC BEGIN
  --  weve sent our messages, and gotten our reactions. now, we wait for totalVoteTime and
  --  and check to see if theyve been approved every so often
  local timedOutCounter = newCounter(BotConfig.Joining.VotingHours)
  while true do
    --  lets calculate our rating
    approved = approvedReaction.count - 1
    declined = declinedReaction.count - 1
    rating = (approved/totalMembers) * 100
    --  first, lets see if the member is still part of our guild
    if DiscordExtensions:isMemberStillInGuild(member.id) == false then
      --  member left on us, cancel the vote and terminate.
      approvalMessage:setContent(BOT_MESSAGES.COMMUNITYAPPROVAL_USER_LEFT_EDIT_VOTE:format(member.name))
      emojiAddedListener:disconnect()
      emojiRemovedListener:disconnect()
      sentNotification:setContent(BOT_MESSAGES.COMMUNITYAPPROVAL_USER_LEFT_EDIT_DM)
      return false, USER_FAILED_RESPONSE.ERROR_LEFT_MID_PROCESS, sentNotification
    end
    --  check to see if our member has been approved
    if rating >= BotConfig.Joining.ApprovalPercentage then
      --  CONGRATULATIONS! the new member has been approved by the community,
      --  let them know.
      emojiAddedListener:disconnect()
      sentNotification:delete()
      approvalMessage:delete()
      --  todo: wipe save data
      return true
    end
    --  second, lets see how long the votes been going on for. we can only run for BotConfig.Joining.VotingHours
    if timeRemaining <= 0 then
      --  weve ran out of time, terminate
      coroutine.resume(coroutine.create(
        function()
          approvalMessage:setContent(
            BOT_MESSAGES.COMMUNITYAPPROVAL_DECLINED_EDIT_VOTE:format(
              member.id,
              rating
            )
          )
          approvalMessage:delete()
        end
      ))
      emojiAddedListener:disconnect()
      emojiRemovedListener:disconnect()
      sentNotification:delete()
      DiscordExtensions:sendNotificationToMember(
        member,
        BOT_MESSAGES.ENTRY_DECLINED:format(rating),
        true
      )
      return false, USER_FAILED_RESPONSE.ENTRY_DECLINED:format(rating)
    end
    --  remove 60 seconds from timeRemaining, and wait for 60 seconds
    if BotConfig.TestMode then
      timeRemaining = timeRemaining - 4
      wait(10)
    else
      timeRemaining = timeRemaining - 60
      wait(60)
    end
  end
end
--  table  stepsRequired
--    a table for processNewMember
local stepsRequired = {
  performTOSCheck,
  performUserVerification,
  performLuaTest,
  performRequireGroup,
  performCommunityCheck
}
--  void  processNewMember(newMember)
--    Performs the joining process for newMember
local function processNewMember(newMember)
  --  first, lets check to see if the player has "leftData"
  local leftData = UserData:get(
    newMember.id,
    "leftData"
  )
  if leftData ~= nil then
    --  user has previously joined. lets see if they meet our Joining.MustRetakeJoiningTestAfterSetDays to see
    --  if we need to make them retake the joining process.
    if leftData.time < (os.time() + (BotConfig.Joining.MustRetakeJoiningTestAfterSetDays *86400)) then
      --  user is within time-frame, let them rejoin.
      --  reapply all of the roles they previously had
      for key, roleId in next, leftData.roles do
        newMember:addRole(roleId)
      end
      --  welcome them back
      DiscordExtensions:sendNotificationToMember(
        newMember,
        BOT_MESSAGES.JOINING_WELCOME_BACK:format(newMember.user.mentionString),
        true
      )
      return
    end
    --  if we got here, we'll have to make them retake the test as theyve been gone for too long.
    --  go ahead and wipe leftData
    UserData:set(
      newMember.id,
      "leftData",
      nil
    )
    DiscordExtensions:sendNotificationToMember(
      newMember,
      BOT_MESSAGES.JOINING_RETEST_REQUIRED:format(newMember.user.mentionString),
      true
    )
  end
  ----  variables
  local stepSuccess, failureReason, sentNotification
  --  third, lets check to see if the user was in the middle of taking the test when they left.
  local joiningProcessData = UserData:get(
    newMember.id,
    "joiningProcessData"
  )
  if joiningProcessData ~= nil then
    --  user has previously attempted to take a test, and either left or timed-out. lets see if they
    --  qualify to continue off from where they started.
    if joiningProcessData.time > (os.time() + (BotConfig.Joining.MustRetakeJoiningTestAfterSetDays * 86400)) then
      --  user doesnt qualify, remove data, alert them, and wipe UserData key
      joiningProcessData = UserData:set(
        newMember.id,
        "joiningProcessData",
        {}
      )
      DiscordExtensions:sendNotificationToMember(
        newMember,
        BOT_MESSAGES.JOINING_RETEST_REQUIRED,
        true
      )
    else
      --  user qualifies, alert them
      if joiningProcessData.version == nil then
        --  we're using old joiningProcessData, alert them of the conversion and convert
        local conversionMessage = DiscordExtensions:sendNotificationToMember(
          newMember,
          "**Time to convert!**\n\nLook's like you're utilizing an old data format for the joining process! Before we can continue, PigBot must update your data to the new format. This will only take a second ..."
        )
        local votingMessageId = UserData:get(
          newMember.id,
          "votingMessageId"
        )
        if votingMessageId ~= nil then
          joiningProcessData.joining_approvalMessage = votingMessageId
          UserData:set(
            newMember.id,
            "votingMessageId",
            nil
          )
        end
        local usersWhoveVoted = UserData:get(
          newMember.id,
          "usersWhoveVoted"
        )
        if usersWhoveVoted ~= nil then
          joiningProcessData.usersWhoveVoted = usersWhoveVoted
          UserData:set(
            newMember.id,
            "usersWhoveVoted",
            nil
          )
        end
        Debugging.write("finalizing convert")
        votingMessageId = nil
        usersWhoveVoted = nil
        joiningProcessData.version = 1
        Debugging.write("update complete!")
        conversionMessage:setContent("**Conversion complete!**\n\nYour joining data is now utilizing the new format. Continuing process ...")
      end
      DiscordExtensions:sendNotificationToMember(
        newMember,
        BOT_MESSAGES.JOINING_RESUMING_TEST:format(joiningProcessData.leftOffAt),
        true
      )
    end
  else
    joiningProcessData = UserData:set(
      newMember.id,
      "joiningProcessData",
      {
        time = os.time()
      }
    )
  end
  --  welcomeMessage
  --    attempt to get the original welcomeMessage so we dont have to send another notification
  local welcomeMessage = joiningProcessData.welcomeMessage
  if welcomeMessage ~= nil then
    --  we got an id, try to retrieve a message.
    welcomeMessage = newMember.user:getPrivateChannel():getMessage(joiningProcessData.welcomeMessage)
    if welcomeMessage == nil then
      Debugging.write(
        "previously saved welcomeMessage %s does not exist for %s! creating new one",
        joiningProcessData.welcomeMessage,
        newMember.username
      )
    end
  end
  --  if welcomeMessage is nil, create a new one
  if welcomeMessage == nil then
    welcomeMessage = DiscordExtensions:sendNotificationToMember(
      newMember,
      BOT_MESSAGES.JOINING_WELCOME,
      true
    )
  end
  joiningProcessData.welcomeMessage = welcomeMessage.id
  --  get our linked robloxUserId
  --  TODO: implement new verification system and clean up ugly code
  local robloxUserId = UserData:get(
    newMember.id,
    "robloxUserId"
  )
  ----  application process loop.
  ----    little bit of magic to reduce how much code i have to write
  local loopStart = joiningProcessData.leftOffAt or 1
  for i = loopStart, 5 do
    --  update joining process data
    joiningProcessData.leftOffAt = i
    --  now do the loop
    if i == 1 then
      stepSuccess, failureReason, sentNotification = performTOSCheck(newMember)
    elseif i == 2 then
      stepSuccess, failureReason, sentNotification = performUserVerification(newMember)
      if stepSuccess then
        robloxUserId = failureReason
      end
    elseif i == 3 then
      stepSuccess, failureReason, sentNotification = performLuaTest(newMember)
    elseif i == 4 then
      if robloxUserId == nil then
        stepSuccess, failureReason, sentNotification = performUserVerification(newMember)
        if stepSuccess then
          robloxUserId = failureReason
          stepSuccess, failureReason, sentNotification = performRequireGroup(newMember, robloxUserId)
        end
      else
        stepSuccess, failureReason, sentNotification = performRequireGroup(newMember, robloxUserId)
      end
    elseif i == 5 then
      if robloxUserId == nil then
        stepSuccess, failureReason, sentNotification = performUserVerification(newMember)
        if stepSuccess then
          robloxUserId = failureReason
          stepSuccess, failureReason, sentNotification = performCommunityCheck(newMember, robloxUserId)
        end
      else
        stepSuccess, failureReason, sentNotification = performCommunityCheck(newMember, robloxUserId)
      end
    end
    joiningProcessData.leftOffAt = i
    --  check to see if we should continue
    if stepSuccess == false then
      --  looks like we failed a step, boogy out of here.
      if failureReason == USER_FAILED_RESPONSE.ERROR_USER_TIMED_OUT or failureReason == USER_FAILED_RESPONSE.ERROR_LEFT_MID_PROCESS  or failureReason == USER_FAILED_RESPONSE.KNOWLEDGE_TEST_DECLINED or failureReason == USER_FAILED_RESPONSE.ERROR_COULDNT_FIND_BOOLEAN then
        --  user either left or timed out on us. data prep is the same, regardless
        joiningProcessData.time = os.time()
        joiningProcessData.leftOffAt = i
        UserData:set(
          newMember.id,
          "joiningProcessData",
          joiningProcessData
        )
        --  actually check failureReason for correct response
        if failureReason == USER_FAILED_RESPONSE.ERROR_LEFT_MID_PROCESS then
          sentNotification:setContent(BOT_MESSAGES.ERROR_LEFT_MID_PROCESS)
        elseif failureReason == USER_FAILED_RESPONSE.ERROR_USER_TIMED_OUT then
          sentNotification:setContent(BOT_MESSAGES.ERROR_USER_TIMED_OUT)
        end
      else
        --  otherwise, wipe joiningProcessData as theyve failed a criticial part of the test.
        UserData:set(
          newMember.id,
          "joiningProcessData",
          nil
        )
        if BotConfig.TestMode == false then
          welcomeMessage:delete()
          newMember:ban(failureReason, BotConfig.Joining.DeclinedBanTime)
          return
        end
      end
      welcomeMessage:delete()
      --  kick the user
      newMember:kick(failureReason)
      return
    end
  end
  --  congrats! the user has passed all tests, give them their roles and
  --  alert them of the good news.
  newMember:addRole(BotConfig.Joining.RoleToAssign)
  DiscordExtensions:sendNotificationToMember(
    newMember,
    BOT_MESSAGES.ENTRY_ACCEPTED:format(newMember.user.mentionString),
    true
  )
  UserData:set(
    newMember.id,
    "joiningProcessData",
    nil
  )
  UserData:set(
    newMember.id,
    "votingMessageId",
    nil
  )
  UserData:set(
    newMember.id,
    "usersWhoveVoted",
    nil
  )
  UserData:set(
    newMember.id,
    "hasReceivedAlert",
    nil
  )
end
------  FINALIZATION
Client:on("memberJoin", processNewMember)
ready()
--  automatically reconnect any members that are being processed
pbReady:wait()
for _, member in next, GUILD.members[2] do
  --  make sure the user is either a bot or doesn't have the RoleToAssign before anything else
  if member.user.bot == false and member:hasRole(BotConfig.Joining.RoleToAssign) == false then
    --  alert user of improper shutdown, and tell them to rejoin.
    -- DiscordExtensions:sendNotificationToMember(member, BOT_MESSAGES.ERROR_BAD_SHUTDOWN:format(BotConfig.ReasonForBadShutdown))
    Debugging.write(tostring(member) .. member.username)
    Debugging.write(
      "beginning processNewMember for %s",
      member.username
    )
    spawn(
      function()
        processNewMember(member)
      end
    )
  end
end
