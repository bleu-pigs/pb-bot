# PB-Bot:  official PigBot bot application for Bleu Pigs
PigBot Bot is the official open source management application for the Bleu Pigs community. It ranges from a variety of tasks and operations, including but not limited to:
-  Automated community joining and approval process:
<br> Provides a fair and properly enforced joining process. Helps to drastically reduce trolls and potentially toxic members through community-enforced review and voting.

-  Automated Elite Pig application and approval process:
<br>  Follows extremely similar to the joining process knowledge tests, expect with some balance twists.

-  Community-enforced moderation and review:
<br>  Users can be booted out from the community through an flag and review process.

-  Tons of more in the future:
<br>  The above is only what will come soon. I have a ton of things planned out for this system, ranging from member-only API/services, voting for how pigbot enforces policies, and the ability to vote up ideas the community wants implemented.
